import matplotlib.pyplot as plt

from random_moves import RandomMoves


# random moves.
r = RandomMoves(700)
r.moves()
    
# graf
plt.plot(r.x, r.y)
        
# color of first and last points.
plt.scatter(0, 0, c='blue', s=75)
plt.scatter(r.x[-1], r.y[-1], c='red', s=75)
        
#make plot image
plt.savefig("particle on river.png")

plt.show()
    


from matplotlib import pyplot as plt
import numpy as np

#define random positions
rng = np.random.RandomState(0)
x = rng.randn(1000)
y = rng.randn(1000)

#define scatter
plt.scatter(x, y,  c='red', s=10, alpha=0.4)

#make plot image
plt.savefig("particle.png")

# show plot
plt.show()




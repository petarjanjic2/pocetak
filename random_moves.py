from random import choice

#class of random moves.
class RandomMoves():
    
# 0,0 is start
 
    def __init__(self, points=300):

        self.points = points
        

        self.x = [0]
        self.y = [0]

#define moves
    def moves(self):

        
#lenght

        while len(self.x) < self.points:
            
#direction and distance in that direction
            Xdir = choice([1, 0])
            Xdist = choice([0, 3])
            Xmove = Xdir * Xdist
            
            Ydir = choice([1, -1])
            Ydist = choice([0, 3])
            Ymove = Ydir * Ydist
            
          
            
# next move
            x2= self.x[-1] + Xmove
            y2 = self.y[-1] + Ymove
            
            self.x.append(x2)
            self.y.append(y2)

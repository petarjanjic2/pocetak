from matplotlib import pyplot as graf
import numpy as np

#define function
x = np.arange(-20.0, 20.0, 0.001)
y = np.cos(2*np.pi*x) + x**2
graf.plot(x, y)

#define plot
graf.scatter(x, y, color='green', s=7)

#define plot look
graf.title("Grafik trigonometrijske funkcije", fontsize=20)
graf.xlabel('Apscisa', fontsize=12)
graf.ylabel('Ordinata', fontsize=12)
graf.tick_params(axis='both', labelsize=10)
graf.axis([-4, 4, -2, 15])
graf.grid(True)

#make plot image
graf.savefig("grafik.png")

# show plot
graf.show()
